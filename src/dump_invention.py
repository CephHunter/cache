import io
import zlib
import sqlite3

from reads import *
import utils

indexNumber = 2
archiveNumber = 41

def decode(file):
	if readUByte(file) == 0:
		return {}
	objects = {}
	size = readUByte(file)
	while True:
		index = readUByte(file)
		if index == 255:
			break
		amount = readUByte(file)
		types = [readSmarts(file) for _ in range(amount)]
		count = readSmarts(file)
		inner = []
		for _ in range(count):
			obj = []
			for type in types:
				if type in [36]:
					obj.append(readString(file))
				else:
					obj.append(readInt(file))
			if len(obj) == 1:
				obj = obj[0]
			inner.append(obj)
		if len(inner) == 1:
			inner = inner[0]
		objects[index] = inner
	return objects

def get():
	invention = []
	for file in utils.getFiles(indexNumber, archiveNumber):
		decoded = decode(file)
		invention.append(decoded)
	return invention
