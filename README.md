## For windows
### Installing non standard libraries
run `pip install requests pillow`
### Downloading cache
* open cmd with administrative privileges
* change the directory to the `src` folder with `cd <path to project>/src`
* type `python refresh.py` in the command line
* The download can sometimes stop with an error, you can ignore this and just restart the script to continue downloading
### extracting the downloaded cache
open [dump_all.py](./src/dump_all.py) in an editor, you can uncomment
```
# dump_enums.dump()
# dump_structs.dump()
# dump_world_map_mini.dump()
# dump_world_map_bigger.dump()
# dump_sprites.dump()
```
if you want those files as well, note that those take a long time to complete and are thereby commented out by default

Then type `python dump_all.py` in the command line to generate the files.