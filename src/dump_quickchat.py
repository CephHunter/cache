import io
import zlib
import sqlite3

from reads import *
import utils

indexNumber = 24
archiveNumber = 1

opcodes = utils.getOpcodes('opcodes/quickchat.json')

def decode(file):
	obj = {}
	while True:
		op = readUByte(file)
		if op == 0:
			break
		elif op == 3:
			count = readUByte(file)
			params = []
			for _ in range(count):
				type = readUShort(file)
				if type == 0:
					# list
					value = readUShort(file)
				elif type == 1:
					# item
					value = None
				elif type == 4:
					# skill level
					value = readUShort(file)
					remaining = len(file.read())
					file.seek(-remaining, 1)
					if remaining > 1 and readUShort(file) == 11:
						# weird case
						value = ((type, value), 
							(11, readUShort(file)),
							readUShort(file),
							(readUShort(file), readInt(file)))
						break
					else:
						# revert
						file.seek(-2, 1)
				elif type == 6:
					# slayer
					value = readInt(file)
				elif type == 8:
					# counting
					value = readUShort(file)
				elif type == 9:
					value = readUShort(file)
				elif type == 10:
					# tradeable items
					value = None
				elif type == 12:
					# friends chat
					value = None
				elif type == 14:
					# soul wars
					value = readUShort(file)
				elif type == 15:
					# combat level
					value = None
				elif type == 16:
					# collected
					value = readInt(file)
				else:
					raise ValueError("No matching type for %d: %s, %s" % (type, file.getvalue(), list(file.getvalue())))
				params.append([type, value])
			obj['params'] = params

		elif op in opcodes:
			utils.insert(obj, opcodes[op], file)
		else:
			print('fk', op, [i for i in file.read()], obj)
			break
	return obj

def get():
	qc = []
	for file in utils.getFiles(indexNumber, archiveNumber):
		decoded = decode(file)
		qc.append(decoded)
	return qc
