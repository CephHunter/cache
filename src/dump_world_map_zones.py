import io
import zlib
import sqlite3

from reads import *
import utils

indexNumber = 23
archiveNumber = 0
 
# general info about the region
def decode(file):
	obj = {}
	obj['internal_name'] = readString(file)
	obj['external_name'] = readString(file)
	loc = readInt(file)
	obj['loadY'] = loc & 0x3fff
	obj['loadX'] = (loc >> 14) & 0x3fff
	obj['plane'] = loc >> 28
	obj['unk_1'] = readInt(file)
	obj['actuallyShow'] = readUByte(file)
	obj['zoom'] = readUByte(file)
	obj['unk_3'] = readUByte(file)
	obj['pastes'] = []
	size = readUByte(file)
	for _ in range(size):
		sub = {}
		sub['plane'] = readUByte(file)
		sub['oldLL'] = (readUShort(file), readUShort(file))
		sub['oldTR'] = (readUShort(file), readUShort(file))
		sub['newLL'] = (readUShort(file), readUShort(file))
		sub['newTR'] = (readUShort(file), readUShort(file))
		obj['pastes'].append(sub)
	return obj

def get():
	zones = []
	for file in utils.getFiles(indexNumber, archiveNumber):
		zones.append(decode(file))
	return zones
