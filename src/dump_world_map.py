import io
import zlib
import sqlite3

from reads import *
import utils

indexNumber = 23
 
# general info about the region
def decode0(file):
	obj = {}
	obj['internal_name'] = readString(file)
	obj['external_name'] = readString(file)
	loc = readInt(file)
	obj['loadY'] = loc & 0x3fff
	obj['loadX'] = (loc >> 14) & 0x3fff
	obj['plane'] = loc >> 28
	obj['unk_1'] = readInt(file)
	obj['actuallyShow'] = readUByte(file)
	obj['zoom'] = readUByte(file)
	obj['unk_3'] = readUByte(file)
	obj['pastes'] = []
	size = readUByte(file)
	for _ in range(size):
		sub = {}
		sub['plane'] = readUByte(file)
		sub['coord1'] = (readUShort(file), readUShort(file))
		sub['coord2'] = (readUShort(file), readUShort(file))
		sub['coord3'] = (readUShort(file), readUShort(file))
		sub['coord4'] = (readUShort(file), readUShort(file))
		obj['pastes'].append(sub)
	return obj

# which map squares/chunks matter
def decode1(file):
	squares = readUShort(file)
	ret = {}
	if squares != 0:
		ret['squares'] = []
		for _ in range(squares):
			obj = {}
			obj['planes'] = readUShort(file)
			obj['original_regionX'] = readUShort(file)
			obj['original_regionY'] = readUShort(file)
			obj['zero'] = readUByte(file)
			obj['new_regionX'] = readUShort(file)
			obj['new_regionY'] = readUShort(file)
			ret['squares'].append(obj)
	chunks = readUShort(file)
	ret['chunks'] = []
	for _ in range(chunks):
		obj = {}
		obj['planes'] = readUShort(file)
		obj['original_regionX'] = readUShort(file)
		obj['original_regionY'] = readUShort(file)
		obj['original_chunkX'] = readUByte(file)
		obj['original_chunkY'] = readUByte(file)
		obj['zero'] = readUByte(file)
		obj['new_regionX'] = readUShort(file)
		obj['new_regionY'] = readUShort(file)
		obj['new_chunkX'] = readUByte(file)
		obj['new_chunkY'] = readUByte(file)
		ret['chunks'].append(obj)
	ret['width'] = readUByte(file)
	ret['height'] = readUByte(file)
	return ret

# zoomy map?
def decode2(file):
	b = file.read()
	with open('out.png', 'wb') as f:
			f.write(b)

# colors? probably not important, doesn't map directly to any regions
def decode3(file):
	print(list(file.read()))

# still missing some stuff after the png -- more images? different format?
def decode4(file):
	size = readInt(file)
	# _ = readBytes(file, 16)
	# obj['width'] = readInt(file)
	# obj['height'] = readInt(file)
	# print(obj)
	b = file.read(size)
	with open('out.png', 'wb') as f:
			f.write(b)
	b = file.read()
	if len(b) > 1000000:
		print(str(b[:500]), 'a', len(b))
