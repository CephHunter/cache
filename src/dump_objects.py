import io
import zlib
import sqlite3

from reads import *
import utils

indexNumber = 16

opcodes = utils.getOpcodes('opcodes/objects.json')

def decode(file, id):
	obj = {}
	while True:
		op = readUByte(file)
		if op == 0:
			break
		elif op in opcodes:
			utils.insert(obj, opcodes[op], file)
		else:
			print('fk', op, [i for i in file.read()], obj)
			break
	obj['id'] = id
	return obj

def get():
	id = 0
	objects = []
	for archiveNumber, files in utils.getAllFiles(indexNumber):
		for file in files:
			decoded = decode(file, id)
			id += 1
			objects.append(decoded)
	return objects