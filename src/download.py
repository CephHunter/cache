import socket
import struct
import zlib
import lzma
import gzip
import bz2
import io
import requests
import time

def sendSafe(socket, payload):
    # add retry/timeout maybe?
    return socket.send(payload)

def sendInt(socket, payload):
    b = payload.to_bytes(4, byteorder='big')
    return sendSafe(socket, b)

def sendByte(socket, payload):
    b = payload.to_bytes(1, byteorder='big')
    return sendSafe(socket, b)

def sendString(socket, payload):
    b = bytes(payload, encoding='utf-8')
    b = b + b'\x00'
    return sendSafe(socket, b)

def readSafe(socket, n, stream):
    # add retry/timeout maybe?
    remaining = n
    while remaining > 0:
        recvd = socket.recv(remaining)
        stream.write(recvd)
        remaining -= len(recvd)

def readSafeNoStream(socket, n):
    # add retry/timeout maybe?
    data = []
    remaining = n
    while remaining > 0:
        recvd = socket.recv(remaining)
        data.append(recvd)
        remaining -= len(recvd)
    return b''.join(data)

def readByte(socket):
    return ord(socket.recv(1))

def readInt(socket):
    return struct.unpack(">I", readSafeNoStream(socket, 4))[0]


def getParams():
    url = 'http://world3.runescape.com/jav_config.ws'
    content = requests.get(url).content
    params = {}
    for line in content.decode('latin-1').strip().split('\n'):
        key, val = line.rsplit('=', 1)
        params[key] = val
    return params

sock = None

def setup():
    params = getParams()

    TCP_IP = 'content.runescape.com'
    TCP_PORT = 43594

    handshake = 15

    # get this from the source of world2.runescape.com
    fit = [v for k, v in params.items() if len(v) == 32 and 'param=' in k]
    if len(fit) != 1:
        raise ValueError("Failed to get key")
    key = fit[0]
    length = 4 + 4 + 1 + len(key) + 1
    majorVersion = 893
    minorVersion = 1
    language = 0
    loadingRequirements = 27

    while True:
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((TCP_IP, TCP_PORT))
        s.settimeout(5.0)

        sendByte(s, handshake)
        sendByte(s, length)
        sendInt(s, majorVersion)
        sendInt(s, minorVersion)
        sendString(s, key)
        sendByte(s, language)
        
        status = ord(s.recv(1))
        
        if status == 6:
            majorVersion += 1
        elif status == 0:
            break
        else:
            raise ValueError("bad status %s"%status)
    for i in range(108):
        s.recv(1)

    magic = b'\x06\x00\x00\x04\x00\x00\x03\x00\x00\x00\x00\x00'
    s.send(magic)
    return s

def downloadArchive(indexNumber, archiveNumber, filename):
    batchDownload([(indexNumber, archiveNumber, filename, -1)])

def batchDownload(files):
    global sock
    if sock is None:
        sock = setup()
    LIMIT = 800000
    chunks = []
    chunk = []
    totalSize = 0
    for indexNumber, archiveNumber, filename, size in files:
        if size > LIMIT or size < 0:
            chunks.append(chunk)
            chunks.append([(indexNumber, archiveNumber, filename, size)])
            totalSize = 0
            chunk = []
        elif totalSize + size > LIMIT:
            chunks.append(chunk)
            chunk = [(indexNumber, archiveNumber, filename, size)]
            totalSize = size
        else:
            chunk.append((indexNumber, archiveNumber, filename, size))
            totalSize += size
    chunks.append(chunk)
    totRequests = 0
    for chunk in chunks:
        if len(chunk) == 0:
            continue
        requests = {}
        totalLength = 0
        totalRead = 0
        remainingRequests = 0
        for indexNumber, archiveNumber, filename, size in chunk:
            sendByte(sock, 1)
            sendByte(sock, indexNumber)
            sendInt(sock, archiveNumber)
            requests[indexNumber, archiveNumber] = {'stream': io.BytesIO(), 'read': 0, 'filename': filename}
            totalLength += size
            remainingRequests += 1
        while remainingRequests > 0:
            gotIndex = readByte(sock)
            gotArchive = readInt(sock)
            readInChunk = 5
            req = requests[gotIndex, gotArchive]
            if 'compression' not in req:
                req['compression'] = readByte(sock)
                req['compressedSize'] = readInt(sock)
                readInChunk += 5
                if req['compression'] != 0:
                    req['uncompressedSize'] = readInt(sock)
                    readInChunk += 4
            amtToRead = min(102400 - readInChunk, req['compressedSize'] - req['read'])
            readSafe(sock, amtToRead, req['stream'])
            totalRead += amtToRead
            req['read'] += amtToRead
            if req['read'] == req['compressedSize']:
                print(gotIndex, gotArchive)
                remainingRequests -= 1
        totRequests += len(requests)
        for req in requests.values():
            compression = req['compression']
            req['stream'].seek(0)
            data = req['stream'].read()
            if compression == 2:
                d = zlib.decompressobj(32 + zlib.MAX_WBITS).decompress(data)
            elif compression == 1:
                d = bz2.decompress(b'BZh1' + data)
            elif compression == 0:
                d = data
            elif compression == 3:
                # d = lzma.LZMADecompressor().decompress(data, max_length=1)
                print("Doesn't support LZMA yet...")
                return
            else:
                print("Unsupported type ", compression)
                return
            if req['filename'] is None:
                print(len(d))
                return
            with gzip.open(req['filename'], 'wb') as f:
                f.write(d)
