import io
import zlib
import sqlite3

from reads import *
import utils

indexNumber = 23
archiveNumber = 2

def dump():
	for i, file in enumerate(utils.getFiles(indexNumber, archiveNumber)):
		b = file.read()
		with open('../out/mini/%s.png' % i, 'wb') as f:
			f.write(b)

