from requests_futures.sessions import FuturesSession
import gzip

from gevent import monkey
monkey.patch_all()

import utils

indexNumber = 40

def getURL(archiveNumber, crc, version):
	return "http://content.runescape.com/ms?m=0&a=%d&g=%d&c=%d&v=%d" % (indexNumber, archiveNumber, crc, version)

infos = utils.getArchiveInfos(indexNumber)

urls = []
filenames = []
for archiveNumber, info in enumerate(infos[0:100]):
	url = getURL(archiveNumber, info['crc'], info['version'])
	# print(info)
	filename = utils.getDownloadFilename(indexNumber, archiveNumber, info['version'])
	urls.append(url)
	filenames.append(filename)

from requests_futures.sessions import FuturesSession

session = FuturesSession()

def batchDownload(urls, filenames):
    responses = [session.get(url).result() for url in urls]
    for response, info, filename in zip(responses, infos, filenames):
    	print(info, response)
    	if response.status_code == 200:
	    	pass

res2 = batchDownload(urls, filenames)
