import io
from PIL import Image
import sqlite3
import zlib

import utils
from reads import *

indexNumber = 8

def decode(archive):
	archive.seek(-2, 2)

	count = readUShort(archive)

	bitmap = (count >> 15) == 1
	count = count & 0x7FFFFFFF
	if bitmap:
		archive.seek(1)
		frame = {
			'index': 0,
			'bitmap': True,
		}
		flags = readUByte(archive)
		frame['alpha'] = (flags & 0x2) != 0
		frame['vertical'] = (flags & 0x1) != 0
		frame['width'] = readUShort(archive)
		frame['height'] = readUShort(archive)

		npixels = frame['width'] * frame['height']
		frame['base'] = [read3ByteInt(archive) for _ in range(npixels)]
		if frame['alpha']:
			frame['mask'] = [readUByte(archive) for _ in range(npixels)]
		return [frame]

	archive.seek(-7 - count * 8, 2)

	bigWidth = readUShort(archive)
	bigHeight = readUShort(archive)
	paletteSize = readUByte(archive) + 1

	minX = [readUShort(archive) for _ in range(count)]
	minY = [readUShort(archive) for _ in range(count)]
	width = [readUShort(archive) for _ in range(count)]
	height = [readUShort(archive) for _ in range(count)]

	archive.seek(-7 - count * 8 - (paletteSize) * 3, 2)

	palette = [readUByte(archive) for _ in range(paletteSize * 3)]

	archive.seek(0)

	frames = []
	for i in range(count):
		frame = {
			'index': i,
			'bitmap': False,
			'minX': minX[i],
			'minY': minY[i],
			'width': width[i],
			'height': height[i],
			'palette': palette,
		}
		npixels = frame['width'] * frame['height']
		flags = readUByte(archive)
		frame['alpha'] = (flags & 0x2) != 0
		frame['vertical'] = (flags & 0x1) != 0
		frame['base'] = [readUByte(archive) for _ in range(npixels)]
		if frame['alpha']:
			frame['mask'] = [readUByte(archive) for _ in range(npixels)]
		frames.append(frame)
	return frames

def makeImage(frame, name=None):
	shape = (frame['width'], frame['height'])
	if frame['vertical']:
		shape = shape[::-1]

	if frame['bitmap']:
		img = Image.new('RGB', shape)
		img.putdata(frame['base'])
	else:
		img = Image.new('P', shape)
		img.putdata(frame['base'])
		img.putpalette(frame['palette'])

	if frame['alpha']:
		layer = Image.new('L', shape)
		layer.putdata(frame['mask'])
		img.putalpha(layer)
	if frame['vertical']:
		img = img.transpose(Image.TRANSPOSE)
	if shape == (0, 0):
		img = Image.new('RGB', (1, 1), (0x7f, 0xff, 0))
	if name is not None:
		img.save(name)
	return img

def dump():
	for archiveNumber, archive in utils.getArchives(indexNumber):
		if archiveNumber % 100 == 0:
			print(archiveNumber)
		frames = decode(archive)
		for frame in frames:
			makeImage(frame, name='../out/sprites/%s-%s.png' % (archiveNumber, frame['index']))
