import io
import zlib
import sqlite3

from reads import *
import utils

indexNumber = 2
archiveNumber = 35

opcodes = utils.getOpcodes('opcodes/quests.json')

def decode(file):
	obj = {}
	while True:
		op = readUByte(file)
		if op == 0:
			if len(file.read()) > 0:
				print('fk broke')
			break
		elif op in opcodes:
			utils.insert(obj, opcodes[op], file)
		else:
			print('fk', op, list(file.read()))
			break
	return obj

def get():
	files = utils.getFiles(indexNumber, archiveNumber)
	quests = []
	for file in files:
		decoded = decode(file)
		quests.append(decoded)
	return quests
