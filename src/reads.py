def readInt(f):
	unsigned = (readUByte(f) << 24) + (readUByte(f) << 16) + (readUByte(f) << 8) + readUByte(f)
	return unsigned & 0x7FFFFFFF

def readSignedInt(f):
	val = (readUByte(f) << 24) + (readUByte(f) << 16) + (readUByte(f) << 8) + readUByte(f)
	if val & 0x80000000:
		val = -0x100000000 + val
	return val

def read3ByteInt(f):
	unsigned = (readUByte(f) << 16) + (readUByte(f) << 8) + readUByte(f)
	return unsigned & 0x7FFFFFFF

def readUByte(f):
	return ord(f.read(1))

def readByte(f):
	u = readUByte(f)
	if u >= 128:
		return u - 256
	return u

def readLong(f):
	unsigned = (readUByte(f) << 56) | (readUByte(f) << 48) | (readUByte(f) << 40) | (readUByte(f) << 32) | (readUByte(f) << 24) | (readUByte(f) << 16) | (readUByte(f) << 8) | readUByte(f)
	return unsigned & 0x7FFFFFFFFFFFFFFF

def readUSmart(f):
	i = readUByte(f)
	f.seek(f.tell()-1)
	if i >= 128:
		return -32768 + readUShort(f)
	return readUByte(f)

def readDecrSmart(f):
	first = readUByte(f)
	if first < 128:
		return first - 1
	return ((first << 8) | readByte(f)) - 32769

def readSmart(f):
	first = readUByte(f)
	if first < 128:
		return first - 64
	return ((first << 8) | readUByte(f)) - 49152

def readSmart32(f):
	i = readUByte(f)
	f.seek(f.tell()-1)
	if i >= 128:
		return readInt(f)
	value = readUShort(f)
	if value == 32767:
		return -1
	return value

def readSmarts(f):
	value = 0
	offset = readUSmart(f)
	while offset == 32767:
		offset = readUSmart(f)
		value += 32767
	value += offset
	return value

def readUShort(f):
	return (readUByte(f) << 8) | readUByte(f)

def read3UByte(f):
	return (readUByte(f) << 16) | (readUByte(f) << 8) | readUByte(f)

def read4UByte(f):
	return (readUByte(f) << 24) | (readUByte(f) << 16) | (readUByte(f) << 8) | readUByte(f)

def read5UByte(f):
	return [readUByte(f) for _ in range(5)]

def readNBytes(n):
	return lambda f: readBytes(f, n)

def readBytes(f, n):
	return [readUByte(f) for _ in range(n)]

def readChar(f):
	return chr(readUByte(f))

def returnTrue(f):
	return True

def returnFalse(f):
	return False

def readString(f):
	string = []
	while True:
		c = readUByte(f)
		if c == 0:
			break
		string.append(chr(c))
	return ''.join(string)

def readPaddedString(f):
	readUByte(f)
	return readString(f)

def readAmbientSound(f):
	return [readUShort(f), readUShort(f), readUShort(f), readUShort(f), readUByte(f)]

def readTable(f):
	table = {}
	count = readUByte(f)
	for _ in range(count):
		isString = readUByte(f) == 1
		key = read3UByte(f)
		value = readString(f) if isString else readInt(f)
		table[key] = value
	return table

# default byte[] readMaskedIndex() throws IOException {
#     int number = this.readUShort();
#     int count = 0;
#     for(int c=number; c > 0; c >>= 1)
#         count++;
#     int value = 0;
#     byte[] result = new byte[count];

#     for(int i=0; i < count; i++){
#         if((number&(0x01<<i)) > 0)
#             result[i] = (byte) value++;
#         else
#             result[i] = -1;
#     }
#     return result;
def readMaskedIndex(f):
	# TODO
	return readUShort(f)

def readMorphTable(f):
	table = {}
	table['varbit'] = readUShort(f)
	table['varp'] = readUShort(f)
	count = readUByte(f)
	table['ids'] = [readUShort(f) for _ in range(count+1)]
	return table

def readExtendedMorphTable(f):
	table = {}
	table['varbit'] = readUShort(f)
	table['varp'] = readUShort(f)
	last = readUShort(f)
	count = readUByte(f)
	table['ids'] = [readUShort(f) for _ in range(count + 1)]
	table['ids'].append(last)
	return table

def readObjectMorphTable(f):
	table = {}
	table['varbit'] = readUShort(f)
	table['varp'] = readUShort(f)
	count = readUByte(f)
	table['ids'] = [readSmart32(f) for _ in range(count+1)]
	return table

def readObjectExtendedMorphTable(f):
	table = {}
	table['varbit'] = readUShort(f)
	table['varp'] = readUShort(f)
	last = readSmart32(f)
	count = readUByte(f)
	table['ids'] = [readSmart32(f) for _ in range(count + 1)]
	table['ids'].append(last)
	return table

def readBitMaskedData(f):
	result = []
	mask = readUByte(f)
	while mask > 0:
		if mask & 0x1 == 1:
			result.append([readSmart32(f), readDecrSmart(f)])
		else:
			result.append([-1, -1])
		mask = mask // 2
	return result

def readSequence(f):
	count = readUShort(f)
	return [readUShort(f) for i in range(3*count)]

def readSmallSequence(f):
	count = readUShort(f)
	return [readUShort(f) for i in range(2*count)]

def readSequence1(f):
	count = readUShort(f)
	return [readUShort(f) for i in range(1*count)]

def readSequence13(f):
	ret = []
	count = readUShort(f)
	for i in range(count):
		inner_count = readUByte(f)
		if inner_count == 0:
			ret.append([])
		else:
			ret.append([readUByte(f) for _ in range(inner_count*2 + 1)])
	return ret

# TODO: delete
def read11Bytes(f):
	return [readUByte(f) for _ in range(11)]

def read5Bytes(f):
	return [readUByte(f) for _ in range(5)]

def read12Bytes(f):
	return [readUByte(f) for _ in range(12)]

reads = globals()
def read(name):
	if type(name) == int:
		return lambda f: readBytes(f, name)
	if name in reads:
		return reads[name]
	raise ValueError('%s not found' % name)