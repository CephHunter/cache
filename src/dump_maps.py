from reads import *

def decodeObjects(file):
	objectId = -1
	objectLocations = []
	while True:
		objIncr = readSmarts(file)
		if objIncr == 0:
			break
		objectId += objIncr
		location = 0
		while True:
			locIncr = readUSmart(file)
			if locIncr == 0:
				break
			objectData = readUByte(file)
			thing = {}
			location += locIncr - 1
			thing['objectId'] = objectId
			thing['localX'] = (location >> 6) & 0x3F
			thing['localY'] = location & 0x3F
			thing['plane'] = location >> 12
			thing['type'] = objectData >> 2
			thing['rotation'] = objectData & 0x3
			yield thing

# 0 -> decodeObjects: main land
# 1 -> decodeObjects: water
# 2 -> ???
# 3 -> decodeSquares: main land (4), missing something at the end
# 4 -> decodeSquares: water, likely
def decodeSquares(file, nplanes=4):
	if len(file.getvalue()) < 4096:
		return
	squares = [[[None for y in range(64)] for x in range(64)] for p in range(nplanes)]
	for plane in range(nplanes):
		for x in range(64):
			for y in range(64):
				flags = readUByte(file)
				flag1 = flags & 0x1 != 0
				flag2 = flags & 0x2 != 0
				flag3 = flags & 0x4 != 0
				flag4 = flags & 0x8 != 0
				obj = {}
				if flag1:
					obj['overlayShape'] = readUByte(file)
					obj['overlayTile'] = readUSmart(file)
				if flag2:
					obj['unk2'] = readByte(file)
				if flag3:
					obj['underlayTile'] = readUSmart(file)
				if flag4:
					obj['height'] = readUByte(file)
				squares[plane][x][y] = obj
	return squares
